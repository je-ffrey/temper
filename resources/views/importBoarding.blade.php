@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Import</div>

                <div class="panel-body">

					<form action="postImport" method="post" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="file" name="import_file">
						<input type="submit" class='btn btn-success' value="Import csv"></input>
					</form>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection