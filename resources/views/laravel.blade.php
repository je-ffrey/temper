@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    Welcome  {{ Auth::user()->email }}
                    
                    <!--Grafiek die getoond wordt d.m.v. js in assets/js/main.js
                    <div>
                    <canvas id="myChart" ></canvas>
                    </div>
                    -->
                    <div>
                        <a href="{{URL::to('getImport')}}" class='btn btn-success'>Import csv</a>
                    </div>
                     <div><form>
                                <a href="{{URL::to('destroy')}}" class='btn btn-danger'>Delete csv</a>
                          </form>
                    </div>
                
                    <table class='table table-hover'>
                    		<caption>Retention info</caption>
                    		<thead>
                    			<th>User_id</th>
                    			<th>Created_at</th>
                                <th>Weeknumber</th>
                    			<th>Onboarding_percentage</th>
                    			<th>Count_applications</th>
                    			<th>Count_accepted_applications</th>
                    		</thead>
                    		<tbody>
                                @if($boardings)
                    			@foreach($boardings as $key => $boarding)
                    			<tr id="boarding {{$boarding->user_id}}">
                    				<td> {{$boarding->user_id}} </td>
                    				<td> {{$boarding->created_at}} </td>
                                    <td> {{$boarding->week_number}}</td>
                    				<td> {{$boarding->onboarding_percentage}} </td>
                    				<td> {{$boarding->count_applications}} </td>
                    				<td> {{$boarding->count_accepted_applications}} </td>
                    			</tr>
                    			@endforeach
                                @endif

                    		</tbody>	

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
