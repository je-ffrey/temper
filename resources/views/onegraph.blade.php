@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">One Graph</div>

                <div class="panel-body">
                    Welcome  {{ Auth::user()->email }}
                
                    <div>
                    <canvas id="myChart" ></canvas>
                    </div>
     
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.js"></script>
            <script>                
                var ctx = document.getElementById("myChart");
                var myChart = new Chart(ctx, {
                             type: 'line',
                             data: {

                                    labels:{{ json_encode(array_keys($percentages)) }},
                                    datasets: [
                                                {
                                                    label: "week",
                                                   
                                                    fill: false,
                                                    lineTension: 0,
                                                    backgroundColor: "rgba(75,192,192,0.4)",
                                                    borderColor: "rgba(75,192,192,1)",
                                                    borderJoinStyle: 'miter',
                                                    pointBorderColor: "rgba(75,192,192,1)",
                                                    pointBackgroundColor: "#fff",
                                                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                                                    pointHoverBorderColor: "rgba(220,220,220,1)",
                                                    pointHoverBorderWidth: 2,
                                                    data: {{ json_encode($percentages) }},
                                                    spanGaps: false,
                                                    stepped: false
                                                },
                                      
                                            ]
                                    },
                                              options: {
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero:true,
                                                                type: 'linear'

                                                            }
                                                        }],
                                                        xAxes:[{
                                                            ticks:{
                                                                beginAtZero:true,
                                                                type: 'linear'
                                                            }
                                                        }]
                                                    }
                                                }

                                            });
</script>
@endsection
