@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Graphics combined</div>

                <div class="panel-body">
                    Welcome  {{ Auth::user()->email }}

                    <div>
                    <canvas id="myChart" ></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.js"></script>
            <script>                
                var ctx = document.getElementById("myChart");
                var myChart = new Chart(ctx, {
                             type: 'line',
                             data: {
                                    labels:['0%', '20%', '40%', '50%', '70%', '90%', '99%', '100%'],
                                    datasets: [
                                                {
                                                    label:"Week 29" ,
                                                    fill: false,
                                                    lineTension: 0,
                                                    backgroundColor: "rgba(75,192,192,0.4)",
                                                    borderColor: "rgba(75,192,192,1)",
                                                    borderJoinStyle: 'miter',
                                                    pointBorderColor: "rgba(75,192,192,1)",
                                                    pointBackgroundColor: "#fff",
                                                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                                                    pointHoverBorderColor: "rgba(220,220,220,1)",
                                                    pointHoverBorderWidth: 2,
                                                     data: [100, 82, 45, 33, 30, 20, 15, 0],
                                                    spanGaps: false,
                                                    stepped: false
                                                },
                                                {
                                                    label: "Week 30",
                                                    fill: false,
                                                    lineTension: 0,
                                                    backgroundColor: "rgba(0,0,192,0.4)",
                                                    borderColor: "rgba(0,0,192,1)",
                                                    borderJoinStyle: 'miter',
                                                    pointBorderColor: "rgba(75,192,192,1)",
                                                    pointBackgroundColor: "#fff",
                                                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                                                    pointHoverBorderColor: "rgba(220,220,220,1)",
                                                    pointHoverBorderWidth: 2,
                                                    data: [91, 82, 40, 33, 33, 20, 15, 0],
                                                    spanGaps: false,
                                                },
                                                {
                                                    label: "Week 31",
                                                    fill: false,
                                                    lineTension: 0,
                                                    backgroundColor: "rgba(75,0,192,0.4)",
                                                    borderColor: "rgba(75,0,192,1)",
                                                    borderJoinStyle: 'miter',
                                                    pointBorderColor: "rgba(75,192,192,1)",
                                                    pointBackgroundColor: "#fff",
                                                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                                                    pointHoverBorderColor: "rgba(220,220,220,1)",
                                                    pointHoverBorderWidth: 2,
                                                    data: [100, 66, 55, 33, 31, 22, 10, 5],
                                                    spanGaps: false,
                                                },
                                                {
                                                    label: "Week 32",
                                                    fill: false,
                                                    lineTension: 0,
                                                    backgroundColor: "rgba(75,192,0,0.4)",
                                                    borderColor: "rgba(75,192,0,1)",
                                                    borderJoinStyle: 'miter',
                                                    pointBorderColor: "rgba(75,192,192,1)",
                                                    pointBackgroundColor: "#fff",
                                                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                                                    pointHoverBorderColor: "rgba(220,220,220,1)",
                                                    pointHoverBorderWidth: 2,
                                                    data: [100, 66, 44, 30, 28, 15, 5, 0],
                                                    spanGaps: false,
                                                }
                                      
                                            ]
                                    },
                                              options: {
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero:true,
                                                                type: 'linear'

                                                            }
                                                        }],
                                                        xAxes:[{
                                                            ticks:{
                                                                beginAtZero:true,
                                                                type: 'linear'
                                                            }
                                                        }]
                                                    }
                                                }

                                            });
            </script>

@endsection
