import Chart from 'chart.js';

//Werkt niet vanwege gulp.
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.js"></script>
            <script>                
                var ctx = document.getElementById("myChart");
                var myChart = new Chart(ctx, {
                             type: 'line',
                             data: {

                                    labels:{{ json_encode(array_keys($percentages)) }},
                                    datasets: [
                                                {
                                                    label: {{ $boarding->week_number }},
                                                    fill: false,
                                                    lineTension: 0,
                                                    backgroundColor: "rgba(75,192,192,0.4)",
                                                    borderColor: "rgba(75,192,192,1)",
                                                    borderJoinStyle: 'miter',
                                                    pointBorderColor: "rgba(75,192,192,1)",
                                                    pointBackgroundColor: "#fff",
                                                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                                                    pointHoverBorderColor: "rgba(220,220,220,1)",
                                                    pointHoverBorderWidth: 2,
                                                    data: {{ json_encode($percentages) }},
                                                    spanGaps: false,
                                                    stepped: false
                                                },
                                      
                                            ]
                                    },
                                              options: {
                                                    scales: {
                                                        yAxes: [{
                                                            ticks: {
                                                                beginAtZero:true,
                                                                type: 'linear'

                                                            }
                                                        }],
                                                        xAxes:[{
                                                            ticks:{
                                                                beginAtZero:true,
                                                                type: 'linear'
                                                            }
                                                        }]
                                                    }
                                                }

                                            });
            </script>