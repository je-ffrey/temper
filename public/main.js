import Chart from 'chart.js';

var data = 
    
{
  
  labels:['0%', '20%', '40%', '50%', '70%', '90%', '99%', '100%'],
  
  datasets:[
              {
              data: [99, 89, 70, 70, 20, 20, 15, 10],
              fillColor: ['rgba(220, 220, 220, 0.2)'],
              strokeColor: ['rgba(220,220,220,1)'],
              }
           ],
  
  
};

var context = document.querySelector('#graph').getContext('2d');
new Chart(context).Line(data);

