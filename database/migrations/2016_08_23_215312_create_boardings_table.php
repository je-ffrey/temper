<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boardings', function (Blueprint $table) {
            $table->integer('user_id');
            $table->date('created_at');
            $table->integer('week_number')->nullable();
            $table->integer('onboarding_percentage')->nullable();
            $table->integer('count_applications');
            $table->integer('count_accepted_applications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('boardings');
    }
}
