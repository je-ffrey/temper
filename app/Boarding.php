<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boarding extends Model
{
    public $timestamps = false;

    protected $fillable = [
    	'user_id',
    	'created_at',
    	'week_number',
    	'onboarding_percentage',
    	'count_applications',
    	'count_accepted_applications',
    ];


    
}
