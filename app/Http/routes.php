<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'HomeController@home');	

Route::auth();
Route::get('/getImport', 'ExcelController@getImport')->middleware('admin');
Route::post('/postImport', 'ExcelController@postImport')->middleware('admin');
Route::get('/destroy', 'BoardingController@destroy')->middleware('admin');

Route::get('/dashboard', [
    'as' => 'board', 'uses' => 'BoardingController@index'
])->middleware('admin');

//Route::get('/dashboard/weeknumber/{week_number}','BoardingController@showEachWeek')->middleware('admin');
Route::get('/dashboard/{week_number}','BoardingController@calculateRetention')->middleware('admin');

Route::get('/graph','BoardingController@graph')->middleware('admin');
Route::get('/onegraph/{week_number}','BoardingController@calculateGraph')->middleware('admin');
