<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Boarding;
use DB;

class BoardingController extends Controller
{
    public function index(){
    	
    	$boardings = Boarding::all();
    	return view('laravel', ['boardings'=>$boardings]);
    }

    public function graph(){
        
        $boardings = Boarding::all();
        return view('graph', ['boardings'=>$boardings]);
    }
    // public function showEachWeek($week_number){
    	
    //     $boardings = Boarding::where('week_number', '=', $week_number)->get();
    // 	return view('laravel', ['boardings'=>$boardings]);
    // }
    
    public function calculateRetention($week_number){
                $percentages = array();
                for($i=0; $i<101; $i++){
                    
                    $aantal = Boarding::where('week_number', $week_number)
                                      ->where('onboarding_percentage', '>=', $i)
                                      ->count();

                    $totaal = Boarding::where('week_number', $week_number)->count();
                        
                        if($aantal > 0){

                            $resultaat = $aantal/$totaal*100;
                            $percentages[] = $resultaat;
                        }
                 }
                $boardings = Boarding::where('week_number', $week_number)->get();         
            
            return view('laravel', ['boardings' => $boardings]); 
    }
    
    public function calculateGraph($week_number){
                $percentages = array();
                for($i=0; $i<101; $i++){
                    
                    $aantal = Boarding::where('week_number', $week_number)
                                      ->where('onboarding_percentage', '>=', $i)
                                      ->count();

                    $totaal = Boarding::where('week_number', $week_number)->count();
                        
                        if($aantal > 0){

                            $resultaat = $aantal/$totaal*100;
                            $percentages[] = $resultaat;
                        }
                 }
                $boardings = Boarding::where('week_number', $week_number)->get();         
            
           
            return view('onegraph', ['percentages'=>$percentages, 'boardings' => $boardings]);
            
    }

    public function destroy(){
    	
        DB::table('boardings')->truncate();
    	$boardings = Boarding::all();
    	return view('home', ['boardings'=>$boardings]);
    }

}