<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Boarding;
use Input;
use DB;
use Excel;
use Carbon\Carbon;

class ExcelController extends Controller
{
    public function getImport(){

    	return view('importBoarding');
    }


    public function postImport(){

    	$boardings = Excel::load(Input::file('import_file'),function($reader){
            $reader->each(function($sheet){
                $attributes = $sheet->toArray();
                $attributes['week_number'] = Carbon::createFromFormat('d-m-y', $attributes['created_at'])->weekOfYear;
                Boarding::firstOrCreate($attributes);
            });
        });
        
        return redirect()->route('board');
    }
}